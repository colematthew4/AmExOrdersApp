package cole.matthew.amex.interview

import cole.matthew.amex.interview.data.Order
import cole.matthew.amex.interview.data.ProductDAO
import cole.matthew.amex.interview.data.ProductInventoryDAO
import cole.matthew.amex.interview.events.EventEmitter
import cole.matthew.amex.interview.events.ProductOutOfStockEvent
import cole.matthew.amex.interview.repository.ProductInventoryRepository
import cole.matthew.amex.interview.repository.ProductsRepository
import cole.matthew.amex.interview.service.IOrdersService
import cole.matthew.amex.interview.service.IPricingService
import cole.matthew.amex.interview.service.InventoryAwareOrdersService
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.doubles.shouldBePositive
import io.kotest.matchers.throwable.shouldHaveMessage
import io.kotest.property.Arb
import io.kotest.property.arbitrary.long
import io.kotest.property.arbitrary.next
import io.mockk.clearMocks
import io.mockk.coEvery
import io.mockk.confirmVerified
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify

class InventoryAwareOrdersServiceTest : BehaviorSpec({
    lateinit var productsRepository: ProductsRepository
    val pricingService: IPricingService = mockk()

    beforeContainer {
        coEvery { pricingService.getPricingCalculators() } returns emptyList()
    }

    given("a set of products available to order") {
        val products = setOf(
            ProductDAO(name = "apple", price = 0.60),
            ProductDAO(name = "orange", price = 0.25)
        )
        val inventory = products.map { ProductInventoryDAO(it.name, Arb.long(0, 1_000_000).next()) }.toSet()
        val inventoryRepository: ProductInventoryRepository = mockk {
            coEvery { getProductInventory() } returns inventory
        }

        beforeContainer {
            productsRepository = mockk {
                coEvery { getProductSet() } returns products
            }
        }

        `when`("the order claims more product than is available") {
            then("the order service throws an error") {
                val testEmitter = spyk<EventEmitter>()
                val order = Order(inventory.random().let { it.name to it.availableStock.toUInt() + 5u })
                val ordersService: IOrdersService = InventoryAwareOrdersService(
                    productsRepository,
                    pricingService,
                    inventoryRepository,
                    testEmitter
                )

                shouldThrow<IllegalStateException> {
                    ordersService.calculateTotal(order)
                } shouldHaveMessage "Your order contains more product (${order.products.keys.first()}) than is available (${order.products.values.first() - 5u})"

                confirmVerified(testEmitter)
            }
        }

        `when`("the order claims all of a given product") {
            then("the order service emits a ${ProductOutOfStockEvent::class.simpleName}") {
                val testEmitter = spyk<EventEmitter>()
                val order = Order(inventory.random().let { it.name to it.availableStock.toUInt() })
                val ordersService: IOrdersService = InventoryAwareOrdersService(
                    productsRepository,
                    pricingService,
                    inventoryRepository,
                    testEmitter
                )

                ordersService.calculateTotal(order).shouldBePositive()
                verify(exactly = 1) {
                    testEmitter.emitEvent(match<ProductOutOfStockEvent> {
                        it.productName == order.products.keys.first()
                    })
                }
                confirmVerified(testEmitter)
            }
        }

        `when`("the order claims a subset of a given product") {
            val order = Order(inventory.random().let { it.name to it.availableStock.toUInt() / 2u + 1u })

            then("the order service tracks how much inventory is left over") {
                val ordersService: IOrdersService = InventoryAwareOrdersService(
                    productsRepository,
                    pricingService,
                    inventoryRepository,
                    mockk()
                )

                ordersService.calculateTotal(order).shouldBePositive()
                shouldThrow<IllegalStateException> {
                    ordersService.calculateTotal(order)
                } shouldHaveMessage "Your order contains more product (${order.products.keys.first()}) than is available (${inventory.first { 
                    it.name == order.products.keys.first() 
                }.availableStock.toUInt() - order.products.values.first()})"
            }

            then("the order service does not emit a ${ProductOutOfStockEvent::class.simpleName}") {
                val testEmitter = spyk<EventEmitter>()
                val ordersService: IOrdersService = InventoryAwareOrdersService(
                    productsRepository,
                    pricingService,
                    inventoryRepository,
                    testEmitter
                )

                ordersService.calculateTotal(order).shouldBePositive()
                confirmVerified(testEmitter)
            }
        }

        afterContainer {
            clearMocks(productsRepository)
        }
    }

    afterContainer {
        clearMocks(pricingService)
    }
})
