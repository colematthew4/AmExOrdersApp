package cole.matthew.amex.interview

import cole.matthew.amex.interview.data.Order
import cole.matthew.amex.interview.data.ProductDAO
import cole.matthew.amex.interview.data.Promo
import cole.matthew.amex.interview.util.PromoCalculator
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.shouldBe

class PromoCalculatorTest : BehaviorSpec({
    given("No product promotions") {
        val promos = emptyList<Promo>()

        `when`("The calculator receives an order and its calculated price") {
            val promoCalculator = PromoCalculator(promos)
            val order = Order(
                "apple" to 2u
            )
            val calculatedPrice = 1.20

            then("the calculator returns the original price") {
                promoCalculator.calculateTotal(order, calculatedPrice) shouldBe calculatedPrice
            }
        }
    }

    given("One product promotions") {
        val promo = object : Promo() {
            override val description: String
                get() = "test promo"
            override val product: ProductDAO
                get() = ProductDAO(name = "potato", price = 1.0)

            override fun calculate(order: Order): Double = ANSWER

            val ANSWER = 0.42
        }

        `when`("The calculator receives an order and its calculated price") {
            val promoCalculator = PromoCalculator(listOf(promo))
            val order = Order(
                "apple" to 2u
            )
            val calculatedPrice = 1.20

            then("the calculator returns the promo deduction subtracted from the calculated price") {
                promoCalculator.calculateTotal(order, calculatedPrice) shouldBe calculatedPrice - promo.ANSWER
            }
        }
    }

    given("Multiple product promotions") {
        val promos = listOf(
            object : Promo() {
                override val description: String
                    get() = "test promo 1"
                override val product: ProductDAO
                    get() = ProductDAO(name = "russet potato", price = 1.0)

                override fun calculate(order: Order): Double {
                    return order.products[product.name]!!.toInt() * (product.price / 2)
                }
            },
            object : Promo() {
                override val description: String
                    get() = "test promo 2"
                override val product: ProductDAO
                    get() = ProductDAO(name = "gold potato", price = 1.2)

                override fun calculate(order: Order): Double {
                    return order.products[product.name]!!.toInt() * (product.price * 1 / 6)
                }
            }
        )

        `when`("The calculator receives an order and its calculated price") {
            val promoCalculator = PromoCalculator(promos)
            val order = Order(
                promos.first().product.name to 2u,
                promos.last().product.name to 5u
            )
            val calculatedPrice = 2.2

            then("the calculator returns the sum of the promo deductions subtracted from the calculated price") {
                promoCalculator.calculateTotal(order, calculatedPrice) shouldBe 0.2
            }
        }
    }
})
