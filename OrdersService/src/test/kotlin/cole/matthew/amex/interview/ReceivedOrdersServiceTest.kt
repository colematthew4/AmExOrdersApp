package cole.matthew.amex.interview

import cole.matthew.amex.interview.data.Order
import cole.matthew.amex.interview.data.ProductDAO
import cole.matthew.amex.interview.events.EventEmitter
import cole.matthew.amex.interview.events.OrderReceivedEvent
import cole.matthew.amex.interview.repository.ProductsRepository
import cole.matthew.amex.interview.service.EventEmittingOrdersService
import cole.matthew.amex.interview.events.EventSubscriber
import cole.matthew.amex.interview.service.IPricingService
import cole.matthew.amex.interview.service.ReceivedOrdersService
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.doubles.shouldBeBetween
import io.mockk.clearMocks
import io.mockk.coEvery
import io.mockk.confirmVerified
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify

class ReceivedOrdersServiceTest : BehaviorSpec({
    lateinit var productsRepository: ProductsRepository
    val pricingService: IPricingService = mockk()

    beforeContainer {
        coEvery { pricingService.getPricingCalculators() } returns emptyList()
    }

    given("a set of products available to order") {
        val products = setOf(
            ProductDAO(name = "apple", price = 0.60),
            ProductDAO(name = "orange", price = 0.25)
        )

        beforeContainer {
            productsRepository = mockk {
                coEvery { getProductSet() } returns products
            }
        }

        `when`("the client submits an order") {
            val testEventEmitter = spyk<EventEmitter>()
            val order = Order("apple" to 3u)
            val ordersService: EventEmittingOrdersService = ReceivedOrdersService(productsRepository, pricingService, testEventEmitter)

            then("the order service emits an event for every order total calculated") {
                val subscriber = spyk<EventSubscriber>()

                ordersService.subscribe(subscriber)
                ordersService.calculateTotal(order).shouldBeBetween(1.8, 1.8, 0.01)
                verify(exactly = 1) {
                    testEventEmitter.emitEvent(match<OrderReceivedEvent> {
                        it.description.startsWith("Your order has been processed!")
                    })
                    testEventEmitter.subscribe(subscriber)
                }
                confirmVerified(testEventEmitter)
            }

            afterEach {
                clearMocks(testEventEmitter)
            }
        }

        afterContainer {
            clearMocks(productsRepository)
        }
    }

    afterContainer {
        clearMocks(pricingService)
    }
})
