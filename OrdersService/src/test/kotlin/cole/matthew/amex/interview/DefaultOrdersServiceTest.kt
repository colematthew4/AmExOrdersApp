package cole.matthew.amex.interview

import cole.matthew.amex.interview.data.Order
import cole.matthew.amex.interview.data.ProductDAO
import cole.matthew.amex.interview.data.Promo
import cole.matthew.amex.interview.repository.ProductsRepository
import cole.matthew.amex.interview.service.DefaultOrdersService
import cole.matthew.amex.interview.service.IPricingService
import cole.matthew.amex.interview.util.PromoCalculator
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.inspectors.forAll
import io.kotest.matchers.shouldBe
import io.kotest.matchers.throwable.shouldHaveMessage
import io.mockk.clearMocks
import io.mockk.coEvery
import io.mockk.mockk

class DefaultOrdersServiceTest : BehaviorSpec({
    lateinit var productsRepository: ProductsRepository
    val pricingService: IPricingService = mockk()

    beforeContainer {
        coEvery { pricingService.getPricingCalculators() } returns emptyList()
    }

    given("a set of products available to order") {
        val products = setOf(
            ProductDAO(name = "apple", price = 0.60),
            ProductDAO(name = "orange", price = 0.25)
        )

        beforeContainer {
            productsRepository = mockk {
                coEvery { getProductSet() } returns products
            }
        }

        `when`("the client submits an order") {
            and("the order is empty") {
                val order = Order()
                val ordersService = DefaultOrdersService(productsRepository, pricingService)

                then("the order service returns a zero total") {
                    ordersService.calculateTotal(order) shouldBe 0.0
                }
            }

            and("the order contains a product that has not been priced") {
                val missingProduct = "pear"
                val order = Order(mapOf(
                    "apple" to 1u,
                    "orange" to 2u,
                    missingProduct to 1u
                ))
                val ordersService = DefaultOrdersService(productsRepository, pricingService)

                then("the order service throws an exception") {
                    shouldThrow<NoSuchElementException> {
                        ordersService.calculateTotal(order)
                    } shouldHaveMessage "Order contains a product '$missingProduct' that has not been priced"
                }
            }

            val promoCalc = PromoCalculator(listOf(object : Promo() {
                override val description: String
                    get() = "test promo"
                override val product: ProductDAO
                    get() = products.last()

                override fun calculate(order: Order): Double {
                    return order.products[product.name]!!.toInt() * (product.price / 5)
                }
            }))

            and("the order contains one product") {
                val order = Order("orange" to 1u)

                and("no promotion exists for that product") {
                    val ordersService = DefaultOrdersService(productsRepository, pricingService)

                    then("the order service returns a total equal to that product's price") {
                        ordersService.calculateTotal(order) shouldBe 0.25
                    }
                }

                and("a promotion exists for that product") {
                    val ordersService = DefaultOrdersService(productsRepository, pricingService)
                        .withCalculator(promoCalc)

                    then("the order service returns a total adjusted from that product's price") {
                        ordersService.calculateTotal(order) shouldBe 0.2
                    }
                }
            }

            and("the order contains multiple of the same product") {
                val order = Order(
                    "apple" to 3u,
                    "orange" to 1u
                )

                and("no promotion exists for that product") {
                    val ordersService = DefaultOrdersService(productsRepository, pricingService)

                    then("the order service returns the total for all products in the order") {
                        ordersService.calculateTotal(order) shouldBe 2.05
                    }
                }

                and("a promotion exists for that product") {
                    val ordersService = DefaultOrdersService(productsRepository, pricingService)
                        .withCalculator(promoCalc)

                    then("the order service returns the total adjusted for all products in the order") {
                        ordersService.calculateTotal(order) shouldBe 2.0
                    }
                }
            }
        }

        afterContainer {
            clearMocks(productsRepository)
        }
    }

    given("no available products") {
        val products = emptySet<ProductDAO>()

        beforeContainer {
            productsRepository = mockk {
                coEvery { getProductSet() } returns products
            }
        }

        `when`("the client submits an order") {
            val ordersService = DefaultOrdersService(productsRepository, pricingService)

            then("the order service throws an exception") {
                listOf(Order(), Order("apple" to 1u)).forAll {
                    shouldThrow<IllegalStateException> {
                        ordersService.calculateTotal(it)
                    } shouldHaveMessage "There are no products available to order"
                }
            }
        }

        afterContainer {
            clearMocks(productsRepository)
        }
    }

    afterContainer {
       clearMocks(pricingService)
    }
})
