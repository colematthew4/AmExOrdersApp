package cole.matthew.amex.interview.events

import reactor.core.publisher.Sinks

class ReactiveSinkEventEmitter(
    private val emitter: Sinks.Many<Event> = Sinks.many().unicast().onBackpressureBuffer(),
    private val failureHandler: Sinks.EmitFailureHandler = Sinks.EmitFailureHandler.FAIL_FAST
) : EventEmitter {
    override fun emitEvent(event: Event) {
        emitter.emitNext(event, failureHandler)
    }

    override fun subscribe(actual: EventSubscriber) {
        actual.subscribe(emitter.asFlux())
    }

    override fun close() {
        emitter.emitComplete(Sinks.EmitFailureHandler.FAIL_FAST)
    }
}
