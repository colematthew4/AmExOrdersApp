package cole.matthew.amex.interview.events

import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer
import reactor.core.Disposable
import reactor.core.publisher.ConnectableFlux
import reactor.core.publisher.Flux
import reactor.kafka.receiver.KafkaReceiver
import reactor.kafka.receiver.ReceiverOptions
import reactor.kafka.receiver.ReceiverRecord

interface EventSubscriber {
    fun subscribe(eventFlux: Flux<Event> = Flux.empty())
}

class KafkaEventReceiver(
    private val delegateSubscriber: EventSubscriber,
    topic: String,
    bootstrapServerHost: String = "localhost:9092",
    config: Map<String, Any> = emptyMap()
) : EventSubscriber, AutoCloseable {
    private val kafkaReceiver: KafkaReceiver<String, Event> = KafkaReceiver.create(ReceiverOptions.create<String, Event>(
        defaultKafkaConsumerConfig + mapOf(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG to bootstrapServerHost) + config
    ).subscription(mutableListOf(topic)))

    private lateinit var receiver: ConnectableFlux<ReceiverRecord<String, Event>>
    private var disposable: Disposable? = null

    override fun subscribe(eventFlux: Flux<Event>) {
        if (!this::receiver.isInitialized) {
            receiver = kafkaReceiver.receive().publish()
            disposable = receiver.connect()
        }

        delegateSubscriber.subscribe(receiver.handle<Event?> { record, sink ->
            sink.next(record.value())
            record.receiverOffset().acknowledge()
        }.concatWith(eventFlux).doOnError {
            println(it)
        })
    }

    override fun close() {
        disposable?.dispose()
    }

    companion object {
        val defaultKafkaConsumerConfig: Map<String, Any> = mapOf(
            ConsumerConfig.CLIENT_ID_CONFIG to KafkaEventReceiver::class.simpleName.orEmpty(),
            ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG to StringDeserializer::class.java,
            ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG to KafkaEventSerializerDeserializer::class.java
        )
    }
}


class ReactiveSinkEventReceiver(
    private val delegateSubscriber: EventSubscriber
) : EventSubscriber by delegateSubscriber
