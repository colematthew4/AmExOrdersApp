package cole.matthew.amex.interview.util

import cole.matthew.amex.interview.data.Order
import cole.matthew.amex.interview.data.PriceAdjustment
import cole.matthew.amex.interview.data.Promo

interface PriceCalculator<T : PriceAdjustment> {
    fun calculateTotal(order: Order, total: Double): Double
}

class PromoCalculator(private val promos: Iterable<Promo>) : PriceCalculator<Promo> {
    override fun calculateTotal(order: Order, total: Double): Double {
        return (total - promos.sumOf { it.calculate(order) }).let {
            // reduce to 2 decimal places
            "%.2f".format(it)
        }.toDouble()
    }
}
