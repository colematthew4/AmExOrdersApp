package cole.matthew.amex.interview.service

import cole.matthew.amex.interview.repository.PromotionsRepository
import cole.matthew.amex.interview.util.PriceCalculator
import cole.matthew.amex.interview.util.PromoCalculator
import java.time.LocalDateTime

interface IPricingService {
    suspend fun getPricingCalculators(): Iterable<PriceCalculator<*>>
}

class PricingService(
    private val promotionsRepository: PromotionsRepository
): IPricingService {
    override suspend fun getPricingCalculators(): Iterable<PriceCalculator<*>> {
        return listOf(
            buildPromotionsCalculator()
        )
    }

    private suspend fun buildPromotionsCalculator(): PromoCalculator {
        return PromoCalculator(promotionsRepository.getActivePromos(LocalDateTime.now()))
    }
}
