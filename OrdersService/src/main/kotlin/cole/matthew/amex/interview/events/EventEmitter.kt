package cole.matthew.amex.interview.events

interface EventEmitter : AutoCloseable {
    fun emitEvent(event: Event)
    fun subscribe(actual: EventSubscriber)
}
