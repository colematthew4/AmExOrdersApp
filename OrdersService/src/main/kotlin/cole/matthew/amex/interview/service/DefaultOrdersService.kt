package cole.matthew.amex.interview.service

import cole.matthew.amex.interview.data.Order
import cole.matthew.amex.interview.data.PriceAdjustment
import cole.matthew.amex.interview.events.EventEmitter
import cole.matthew.amex.interview.events.EventSubscriber
import cole.matthew.amex.interview.events.OrderReceivedEvent
import cole.matthew.amex.interview.events.ProductOutOfStockEvent
import cole.matthew.amex.interview.repository.ProductInventoryRepository
import cole.matthew.amex.interview.repository.ProductsRepository
import cole.matthew.amex.interview.util.PriceCalculator
import kotlin.reflect.KClass
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking

interface IOrdersService {
    fun calculateTotal(order: Order): Double
}
interface EventEmittingOrdersService : IOrdersService {
    fun subscribe(actual: EventSubscriber)
}


class DefaultOrdersService(
    productsRepository: ProductsRepository,
    pricingService: IPricingService
) : IOrdersService {
    private var productSet: Map<String, Double>
    var priceCalculators: Map<KClass<*>, PriceCalculator<*>> = mutableMapOf()
        private set

    init {
        runBlocking(Dispatchers.IO) {
            productSet = productsRepository.getProductSet().associate { (name, price) ->
                name to price
            }
            priceCalculators = pricingService.getPricingCalculators().associateBy { it::class }.toMutableMap()
        }
    }

    inline fun <reified T : PriceAdjustment> withCalculator(calculator: PriceCalculator<T>) = apply {
        (priceCalculators as MutableMap)[T::class] = calculator
    }

    override fun calculateTotal(order: Order): Double {
        check(productSet.isNotEmpty()) { "There are no products available to order" }

        return order.products.map { (name, count) ->
            productSet[name]?.let { it * count.toInt() }
                ?: throw NoSuchElementException("Order contains a product '$name' that has not been priced")
        }.sum().let {
            priceCalculators.values.fold(it) { total, calc ->
                calc.calculateTotal(order, total)
            }
        }.takeIf { it > 0 } ?: 0.0
    }
}


class InventoryAwareOrdersService(
    private val ordersService: IOrdersService,
    inventoryRepository: ProductInventoryRepository,
    private val eventEmitter: EventEmitter
) : EventEmittingOrdersService, AutoCloseable by eventEmitter {
    private var availableInventory: MutableMap<String, ULong>

    constructor(productsRepository: ProductsRepository, pricingService: IPricingService, inventoryRepository: ProductInventoryRepository, eventEmitter: EventEmitter)
        : this(DefaultOrdersService(productsRepository, pricingService), inventoryRepository, eventEmitter)

    init {
        runBlocking(Dispatchers.IO) {
            availableInventory = inventoryRepository.getProductInventory().associate { (name, count) ->
                name to count.toULong()
            }.toMutableMap()
        }
    }

    override fun subscribe(actual: EventSubscriber) {
        eventEmitter.subscribe(actual)
    }

    private fun checkInventory(productsToOrder: Map<String, UInt>) {
        productsToOrder.forEach { (name, count) ->
            val inventory = availableInventory.getOrDefault(name, 0u)
            check(inventory >= count) { "Your order contains more product ($name) than is available ($inventory)" }
        }
    }

    override fun calculateTotal(order: Order): Double {
        checkInventory(order.products)

        return ordersService.calculateTotal(order).also {
            order.products.forEach { (name, count) ->
                availableInventory.merge(name, count.toULong()) { current, takenOut ->
                    val remainingInventory = current - takenOut
                    if (remainingInventory.toUInt() == 0u) {
                        eventEmitter.emitEvent(ProductOutOfStockEvent(name))
                    }

                    remainingInventory.takeIf { it > 0u }
                }
            }
        }
    }
}


class ReceivedOrdersService(
    private val ordersService: IOrdersService,
    private val eventEmitter: EventEmitter
) : EventEmittingOrdersService, AutoCloseable by eventEmitter {
    constructor(productsRepository: ProductsRepository, pricingService: IPricingService, eventEmitter: EventEmitter)
        : this(DefaultOrdersService(productsRepository, pricingService), eventEmitter)

    override fun subscribe(actual: EventSubscriber) {
        eventEmitter.subscribe(actual)
    }

    override fun calculateTotal(order: Order): Double {
        return ordersService.calculateTotal(order).also {
            eventEmitter.emitEvent(OrderReceivedEvent(order.receivedEventDesc))
        }
    }
}

private val Order.receivedEventDesc: String
    get() = """
Your order has been processed!

${products.entries.joinToString("") { (name, count) ->
    buildString(30) {
        append(name)
        while (length < capacity() - count.toInt().mod(10)) {
            append(" - ")
        }
        appendLine(count)
    }
}}
""".trimIndent()
