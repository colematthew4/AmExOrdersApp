package cole.matthew.amex.interview.events

import java.time.LocalDateTime
import java.util.Locale
import kotlinx.serialization.Contextual
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
sealed class Event {
    @Contextual
    val time: LocalDateTime = LocalDateTime.now()
    abstract val description: String

    abstract fun accept(visitor: EventVisitor)
}

@SerialName("orderReceived")
@Serializable
data class OrderReceivedEvent(
    override val description: String
) : Event() {
    override fun accept(visitor: EventVisitor) {
        visitor.visit(this)
    }
}

@SerialName("estimatedDelivery")
@Serializable
data class EstimatedDeliveryEvent(
    override val description: String
) : Event() {
    override fun accept(visitor: EventVisitor) {
        visitor.visit(this)
    }
}

@SerialName("productOutOfStock")
@Serializable
data class ProductOutOfStockEvent(
    val productName: String
) : Event() {
    override val description: String = "'${productName.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }}' is out of stock!"

    override fun accept(visitor: EventVisitor) {
        visitor.visit(this)
    }
}
