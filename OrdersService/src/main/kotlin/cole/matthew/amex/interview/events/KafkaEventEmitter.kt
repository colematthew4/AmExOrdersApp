package cole.matthew.amex.interview.events

import com.charleskorn.kaml.Yaml
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import kotlinx.serialization.KSerializer
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.modules.SerializersModule
import kotlinx.serialization.modules.contextual
import kotlinx.serialization.modules.polymorphic
import kotlinx.serialization.modules.subclass
import org.apache.kafka.clients.admin.AdminClient
import org.apache.kafka.clients.admin.AdminClientConfig
import org.apache.kafka.clients.admin.NewTopic
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.Deserializer
import org.apache.kafka.common.serialization.Serializer
import org.apache.kafka.common.serialization.StringSerializer
import reactor.kafka.sender.KafkaSender
import reactor.kafka.sender.SenderOptions
import reactor.kafka.sender.SenderRecord
import reactor.kotlin.core.publisher.toMono

class KafkaEventEmitter(
    private val topic: String,
    bootstrapServerHost: String = "localhost:9092",
    config: Map<String, Any> = emptyMap()
) : EventEmitter {
    internal val kafkaSender: KafkaSender<String, Event> = KafkaSender.create(SenderOptions.create(
        defaultKafkaProviderConfig + mapOf(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG to bootstrapServerHost) + config
    ))

    init {
        AdminClient.create(mapOf(
            AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG to bootstrapServerHost
        )).createTopics(listOf(NewTopic("events", 1, 1)))
    }

    override fun emitEvent(event: Event) {
        kafkaSender.send(SenderRecord.create(
            ProducerRecord(topic, event::class.java.simpleName, event), event
        ).toMono()).subscribe()
    }

    override fun subscribe(actual: EventSubscriber) {
        actual.subscribe()
    }

    override fun close() {
        kafkaSender.close()
    }

    companion object {
        val defaultKafkaProviderConfig: Map<String, Any> = mapOf(
            ConsumerConfig.CLIENT_ID_CONFIG to KafkaEventEmitter::class.simpleName.orEmpty(),
            ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG to StringSerializer::class.java,
            ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG to KafkaEventSerializerDeserializer::class.java
        )
    }
}


class KafkaEventSerializerDeserializer : Serializer<Event>, Deserializer<Event> {
    override fun configure(configs: MutableMap<String, *>?, isKey: Boolean) { /* no-op */ }

    override fun serialize(topic: String, event: Event) = Event.yamlSerializer
        .encodeToString(Event.serializer(), event)
        .encodeToByteArray()

    override fun deserialize(topic: String, data: ByteArray) = Event.yamlSerializer
        .decodeFromString<Event>(data.decodeToString())

    override fun close() { /* no-op */ }
}


val Event.Companion.yamlSerializer: Yaml
    get() = Yaml(serializersModule = SerializersModule {
        polymorphic(Event::class) {
            subclass(OrderReceivedEvent.serializer())
            subclass(EstimatedDeliveryEvent.serializer())
            subclass(ProductOutOfStockEvent.serializer())
        }
        contextual(object : KSerializer<LocalDateTime> {
            override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("LocalDateTime", PrimitiveKind.STRING)

            override fun deserialize(decoder: Decoder) =
                LocalDateTime.parse(decoder.decodeString(), DateTimeFormatter.ISO_DATE_TIME)

            override fun serialize(encoder: Encoder, value: LocalDateTime) =
                encoder.encodeString(value.format(DateTimeFormatter.ISO_DATE_TIME))
        })
    })
