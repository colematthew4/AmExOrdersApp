package cole.matthew.amex.interview.data

import kotlinx.serialization.Serializable

interface PriceAdjustment {
    fun calculate(order: Order): Double
}

@Serializable
abstract class Promo : PriceAdjustment {
    abstract val description: String
    abstract val product: ProductDAO
}
