package cole.matthew.amex.interview.data

import kotlinx.serialization.Serializable

@Serializable
data class ProductDAO(
    val name: String,
    val price: Double
)
