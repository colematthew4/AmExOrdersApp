package cole.matthew.amex.interview.repository

import cole.matthew.amex.interview.data.ProductDAO

interface ProductsRepository {
    suspend fun getProductSet(): Set<ProductDAO>
}
