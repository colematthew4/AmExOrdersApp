package cole.matthew.amex.interview.data

import kotlinx.serialization.Serializable

@Serializable
data class ProductInventoryDAO(
    val name: String,
    val availableStock: Long
)
