package cole.matthew.amex.interview.service

import cole.matthew.amex.interview.events.Event
import cole.matthew.amex.interview.events.EventSubscriber
import cole.matthew.amex.interview.events.EventVisitor
import reactor.core.publisher.Flux

interface IMailService

class MailService(private val eventVisitor: EventVisitor) : IMailService, EventSubscriber {
    override fun subscribe(eventFlux: Flux<Event>) {
        eventFlux.subscribe {
            it.accept(eventVisitor)
        }
    }
}
