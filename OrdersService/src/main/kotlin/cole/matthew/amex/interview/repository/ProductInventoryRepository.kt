package cole.matthew.amex.interview.repository

import cole.matthew.amex.interview.data.ProductInventoryDAO

interface ProductInventoryRepository {
    suspend fun getProductInventory(): Set<ProductInventoryDAO>
}
