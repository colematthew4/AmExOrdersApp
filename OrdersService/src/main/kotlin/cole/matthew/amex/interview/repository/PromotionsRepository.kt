package cole.matthew.amex.interview.repository

import cole.matthew.amex.interview.data.Promo
import java.time.LocalDateTime

interface PromotionsRepository {
    suspend fun getActivePromos(date: LocalDateTime): Iterable<Promo>
}
