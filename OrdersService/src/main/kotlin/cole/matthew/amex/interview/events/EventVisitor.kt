package cole.matthew.amex.interview.events

interface EventVisitor {
    fun visit(event: OrderReceivedEvent) {
        /* no-op */
    }

    fun visit(event: EstimatedDeliveryEvent) {
        /* no-op */
    }

    fun visit(event: ProductOutOfStockEvent) {
        /* no-op */
    }
}
