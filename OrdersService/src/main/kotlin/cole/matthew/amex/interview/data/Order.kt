package cole.matthew.amex.interview.data

data class Order(val products: Map<String, UInt>) {
    constructor(vararg products: Pair<String, UInt>) : this(products.toMap())
}
