group = "cole.matthew.amex.interview"
version = "1.0.0"

val kotestVersion by extra("4.5.0")
val reactorVersion by extra("2020.0.6")

allprojects {
    repositories {
        mavenCentral()
        mavenLocal()
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}
