import cole.matthew.amex.interview.data.Order
import cole.matthew.amex.interview.events.ConsoleEventVisitor
import cole.matthew.amex.interview.events.KafkaEventEmitter
import cole.matthew.amex.interview.events.KafkaEventReceiver
import cole.matthew.amex.interview.repository.YamlProductInventoryRepository
import cole.matthew.amex.interview.repository.YamlResourceProductsRepository
import cole.matthew.amex.interview.repository.YamlResourcePromoRepository
import cole.matthew.amex.interview.service.IPricingService
import cole.matthew.amex.interview.service.InventoryAwareOrdersService
import cole.matthew.amex.interview.service.MailService
import cole.matthew.amex.interview.service.PricingService
import cole.matthew.amex.interview.service.ReceivedOrdersService

fun main(args: Array<String>) {
    val pricingService: IPricingService = PricingService(YamlResourcePromoRepository)
    KafkaEventEmitter("events").use { eventEmitter ->
        val ordersService = InventoryAwareOrdersService(
            ReceivedOrdersService(YamlResourceProductsRepository, pricingService, eventEmitter),
            YamlProductInventoryRepository,
            eventEmitter
        )

        KafkaEventReceiver(MailService(ConsoleEventVisitor), "events").use { mailService ->
            ordersService.subscribe(mailService)

            val order = Order(args.fold(mutableMapOf()) { acc, name ->
                acc.apply {
                    merge(name, 1u, UInt::plus)
                }
            })

            try {
                val total = ordersService.calculateTotal(order)
                println("Your total is: $$total")
            } catch (e: Exception) {
                println("Could not determine total: ${e.message}")
            }
        }

        ordersService.close()
    }
}
