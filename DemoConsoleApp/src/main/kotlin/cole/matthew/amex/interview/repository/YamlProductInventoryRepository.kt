package cole.matthew.amex.interview.repository

import cole.matthew.amex.interview.data.ProductInventoryDAO
import com.charleskorn.kaml.Yaml
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.builtins.SetSerializer
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.modules.SerializersModule
import kotlinx.serialization.modules.contextual

object YamlProductInventoryRepository : ProductInventoryRepository {
    @OptIn(ExperimentalSerializationApi::class, ExperimentalUnsignedTypes::class)
    private val yamlDecoder = Yaml(serializersModule = SerializersModule {
        contextual(ULong.serializer())
    })

    override suspend fun getProductInventory(): Set<ProductInventoryDAO> {
        return javaClass.classLoader.getResource("inventory.yaml")?.let {
            yamlDecoder.decodeFromString(SetSerializer(ProductInventoryDAO.serializer()), it.readText())
        } ?: error("Failed to load product inventory")
    }
}
