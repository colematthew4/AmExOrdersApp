package cole.matthew.amex.interview.repository

import cole.matthew.amex.interview.data.BoGoPromo
import cole.matthew.amex.interview.data.Promo
import cole.matthew.amex.interview.data.ThreeForTwoPromo
import com.charleskorn.kaml.Yaml
import java.time.LocalDateTime
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.modules.SerializersModule
import kotlinx.serialization.modules.polymorphic
import kotlinx.serialization.modules.subclass

object YamlResourcePromoRepository : PromotionsRepository {
    private val yamlPromoDecoder = Yaml(serializersModule = SerializersModule {
        polymorphic(Promo::class) {
            subclass(BoGoPromo.serializer())
            subclass(ThreeForTwoPromo.serializer())
        }
    })

    override suspend fun getActivePromos(date: LocalDateTime): Iterable<Promo> {
        return javaClass.classLoader.getResource("promos.yaml")?.let {
            yamlPromoDecoder.decodeFromString<List<Promo>>(it.readText())
        } ?: error("Failed to load promos")
    }
}
