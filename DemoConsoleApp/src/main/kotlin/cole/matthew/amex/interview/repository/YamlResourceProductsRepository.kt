package cole.matthew.amex.interview.repository

import cole.matthew.amex.interview.data.ProductDAO
import com.charleskorn.kaml.Yaml
import kotlinx.serialization.builtins.SetSerializer

object YamlResourceProductsRepository : ProductsRepository {
    private lateinit var productSetCache: Set<ProductDAO>

    override suspend fun getProductSet(): Set<ProductDAO> {
        if (this::productSetCache.isInitialized) {
            return productSetCache
        }

        return javaClass.classLoader.getResource("products.yaml")?.let {
            Yaml.default.decodeFromString(SetSerializer(ProductDAO.serializer()), it.readText())
        }?.also {
            productSetCache = it
        } ?: error("Failed to load products")
    }
}
