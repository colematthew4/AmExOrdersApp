package cole.matthew.amex.interview.events

object ConsoleEventVisitor : EventVisitor {
    override fun visit(event: OrderReceivedEvent) {
        print(event)
    }

    override fun visit(event: EstimatedDeliveryEvent) {
        print(event)
    }

    override fun visit(event: ProductOutOfStockEvent) {
        print(event)
    }

    private fun print(event: Event) {
        println("${event.time}> ${event.description}")
    }
}
