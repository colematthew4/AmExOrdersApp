package cole.matthew.amex.interview.data

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@SerialName("bogo")
@Serializable
data class BoGoPromo(
    override val description: String,
    override val product: ProductDAO
) : Promo() {
    override fun calculate(order: Order): Double {
        return order.products[product.name]?.toInt()?.div(2)?.takeIf { it > 0 }?.let {
            product.price * it
        } ?: 0.0
    }
}

@SerialName("3f2")
@Serializable
data class ThreeForTwoPromo(
    override val description: String,
    override val product: ProductDAO
) : Promo() {
    override fun calculate(order: Order): Double {
        return order.products[product.name]?.toInt()?.div(3)?.takeIf { it > 0 }?.let {
            product.price * it
        } ?: 0.0
    }
}
