import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.5.0"
    kotlin("plugin.serialization") version "1.5.0"
    application
}

group = "cole.matthew.amex.interview"
version = "1.0.0"

val reactorVersion: String by rootProject.extra

dependencies {
    implementation(project(":OrdersService"))
    implementation("com.charleskorn.kaml:kaml:0.31.0")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:1.2.0")
    implementation(platform("io.projectreactor:reactor-bom:$reactorVersion"))
    implementation("io.projectreactor:reactor-core")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

application {
    mainClassName = "MainKt"
}
